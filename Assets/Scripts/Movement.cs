using UnityEngine;


public class Movement : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private Animator _animator;

    private const string _movementParameter = "speed";

    private float _horizontalMove = 0f;
    private bool _isGrounded = false;
    private bool _lookingRight = true;


    private void Update()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal") * _speed;

        if (_horizontalMove != 0)
        {
            _animator.SetFloat(_movementParameter, 1);
        }
        else
        {
            _animator.SetFloat(_movementParameter, 0);
        }

        if (_horizontalMove < 0 && _lookingRight)
            Flip();
        else if (_horizontalMove > 0 && !_lookingRight)
            Flip();


        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space)) && _isGrounded)
        {
            _rigidbody2D.AddForce(Vector2.up * _jumpForce);
            _isGrounded = false;
        }
    }

    private void FixedUpdate()
    {
        Vector2 targetVelocity = new Vector2(_horizontalMove, _rigidbody2D.velocity.y);
        _rigidbody2D.velocity = targetVelocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.TryGetComponent(out Ground ground))
        {
            _isGrounded = true;
        }
    }

    private void Flip()
    {
        this.gameObject.TryGetComponent(out SpriteRenderer spriteRenderer);
        spriteRenderer.flipX = _lookingRight;

        _lookingRight = !_lookingRight;
    }
}

using UnityEngine;

enum Direction
{
    RIGHT,
    LEFT,
}

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform _spawnPosition;
    [SerializeField] private float _speed;
    [SerializeField] private Transform _positionCheckGround;


    private Collider2D _boxGroundCheck;
    private Vector2 _sizeGroundBoxCheck = new(0, 1);
    private float _angleGroundBoxCheck = 0f;

    private Direction _direction;

    private void Start()
    {
        this.transform.position = _spawnPosition.position;
        _direction = Direction.LEFT;
    }

    private void Update()
    {
        this.transform.Translate(_speed * Time.deltaTime * Vector2.left);
        _boxGroundCheck = Physics2D.OverlapBox(_positionCheckGround.position, _sizeGroundBoxCheck, _angleGroundBoxCheck);
        
        if (_boxGroundCheck == null)
        {
            if (_direction == Direction.LEFT)
            {
                this.transform.eulerAngles = new Vector3(0, -180, 0);
                _direction = Direction.RIGHT;
            }
            else
            {
                this.transform.eulerAngles = new Vector3(0, 0, 0);
                _direction = Direction.LEFT;
            }
        }
    }
}

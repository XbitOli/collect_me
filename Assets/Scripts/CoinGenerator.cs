using UnityEngine;

public class CoinGenerator : MonoBehaviour
{
    [SerializeField] private Transform _spawnPoints;
    [SerializeField] private Coin _prefab;

    private Transform[] _coinSpawnPoints;
    private void Start()
    {
        _coinSpawnPoints = new Transform[_spawnPoints.childCount];

        for (int i = 0; i < _coinSpawnPoints.Length; i++)
        {
            _coinSpawnPoints[i] = _spawnPoints.GetChild(i);
        }

        for (int i = 0; i < _coinSpawnPoints.Length; i++)
        {
            Instantiate(_prefab, _coinSpawnPoints[i].position, Quaternion.identity);
        }
    }
}
